/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.android.settings;

import java.net.Inet4Address;
import java.net.InetAddress;

import android.net.NetworkUtils;

import com.android.settings.R;
import com.android.settings.ProxySelector;

import java.util.regex.Pattern;

import android.content.Context;
import android.preference.EditTextPreference;
import android.provider.Settings.System;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings.System;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.AdapterView;

import android.net.EthernetManager;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.net.ProxyInfo;
import android.net.Uri;

class ethernet_proxy_dialog extends AlertDialog implements TextWatcher,AdapterView.OnItemSelectedListener{
	
	public getStaticIpInfo mGetStaticInfo; 
	private TextView mHostname;
	private TextView mPort;
	private TextView mExclusionlist;
    private TextView mPac;
    private Spinner mProxySettingsSpinner;
    EthernetManager mEthManager;
    IpConfiguration mIpConfig;
    private static final String TAG = "EthernetProxySettings";
    
    public static final int PROXY_NONE = 0;
    public static final int PROXY_STATIC = 1;
    public static final int PROXY_PAC = 2;

	static final int BUTTON_SUBMIT = DialogInterface.BUTTON_POSITIVE;
	static final int BUTTON_FORGET = DialogInterface.BUTTON_NEUTRAL;

	// private final boolean mEdit;
	private final DialogInterface.OnClickListener mListener;

	private View mView;
	Context mcontext;

	// private boolean mHideSubmitButton;

	public ethernet_proxy_dialog(Context context, boolean cancelable,
			DialogInterface.OnClickListener listener,getStaticIpInfo GetgetStaticIpInfo) {
		super(context);
		mcontext = context;
		mListener = listener;
		mGetStaticInfo=GetgetStaticIpInfo;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mView = getLayoutInflater().inflate(R.layout.ethernet_proxy_dialog, null);
		setView(mView);
		setInverseBackgroundForced(true);

		mHostname = (TextView) mView.findViewById(R.id.ethernet_proxy_hostname);
		mPort = (TextView) mView.findViewById(R.id.ethernet_proxy_port);
		mExclusionlist = (TextView) mView.findViewById(R.id.ethernet_proxy_exclusionlist);
        mPac = (TextView) mView.findViewById(R.id.ethernet_proxy_pac);
        mProxySettingsSpinner = (Spinner) mView.findViewById(R.id.ethernet_proxy_settings);

		mHostname.addTextChangedListener(this);
		mPort.addTextChangedListener(this);
		mExclusionlist.addTextChangedListener(this);
        mPac.addTextChangedListener(this);
        mProxySettingsSpinner.setOnItemSelectedListener(this);

		setButton(BUTTON_SUBMIT, mcontext.getString(R.string.ethernet_connect), mListener);
		setButton(BUTTON_NEGATIVE,mcontext.getString(R.string.ethernet_cancel), mListener);
		setTitle(mcontext.getString(R.string.ethernet_proxy));
        
        mEthManager = (EthernetManager) mcontext.getSystemService(Context.ETHERNET_SERVICE);

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();
        updateProxyFields();
	}
	private void updateProxyFields() {
		Log.d(TAG, "Ethernet Proxy status showProxyFields");
        mIpConfig = mEthManager.getConfiguration();
		if (mIpConfig.getProxySettings() == ProxySettings.STATIC) {
            mProxySettingsSpinner.setSelection(PROXY_STATIC);
            setVisibility(R.id.ethernet_proxy_field, View.VISIBLE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.GONE);

            if (mIpConfig != null) {
                ProxyInfo proxyProperties = mIpConfig.getHttpProxy();
                if (proxyProperties != null) {
                    mHostname.setText(proxyProperties.getHost());
                    mPort.setText(Integer.toString(proxyProperties.getPort()));
                    mExclusionlist.setText(proxyProperties.getExclusionListAsString());
                }
            }
        } else if (mIpConfig.getProxySettings() == ProxySettings.PAC) {
            mProxySettingsSpinner.setSelection(PROXY_PAC);
            setVisibility(R.id.ethernet_proxy_field, View.GONE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.VISIBLE);
             if (mIpConfig != null) {
                ProxyInfo proxyInfo = mIpConfig.getHttpProxy();
                if (proxyInfo != null) {
                    mPac.setText(proxyInfo.getPacFileUrl().toString());
                }
            }
        } else {
            setVisibility(R.id.ethernet_proxy_field, View.GONE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.GONE);
        }
	}
    
    private void showProxyFields() {
		Log.d(TAG, "Ethernet Proxy status showProxyFields");
        mIpConfig = mEthManager.getConfiguration();
		if (mProxySettingsSpinner.getSelectedItemPosition() == PROXY_STATIC) {
            setVisibility(R.id.ethernet_proxy_field, View.VISIBLE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.GONE);

            if (mIpConfig != null) {
                ProxyInfo proxyProperties = mIpConfig.getHttpProxy();
                if (proxyProperties != null) {
                    mHostname.setText(proxyProperties.getHost());
                    mPort.setText(Integer.toString(proxyProperties.getPort()));
                    mExclusionlist.setText(proxyProperties.getExclusionListAsString());
                }
            }
        } else if (mProxySettingsSpinner.getSelectedItemPosition() == PROXY_PAC) {
            setVisibility(R.id.ethernet_proxy_field, View.GONE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.VISIBLE);
             if (mIpConfig != null) {
                ProxyInfo proxyInfo = mIpConfig.getHttpProxy();
                if (proxyInfo != null) {
                    mPac.setText(proxyInfo.getPacFileUrl().toString());
                }
            }
        } else {
            setVisibility(R.id.ethernet_proxy_field, View.GONE);
            setVisibility(R.id.ethernet_proxy_pac_field, View.GONE);
        }
	}

	public void saveProxySettingInfo() {
        Log.d(TAG, "save Ethernet Proxy Settings");
        if (mProxySettingsSpinner.getSelectedItemPosition() == PROXY_STATIC) {
            String hostname = mHostname.getText().toString();
            String port = mPort.getText().toString();
            String exclusionlist = mExclusionlist.getText().toString();
            
            mGetStaticInfo.getEthProxySettings("MANNUL");
            mGetStaticInfo.getEthHostname(hostname);
            mGetStaticInfo.getEthPort(port);
            mGetStaticInfo.getEthExclusionlist(exclusionlist);
            
        } else if (mProxySettingsSpinner.getSelectedItemPosition() == PROXY_PAC) {
            String pac = mPac.getText().toString();
            mGetStaticInfo.getEthProxySettings("PAC");
            mGetStaticInfo.getEthPac(pac);
        } else {
            mGetStaticInfo.getEthProxySettings("NONE");
        }
	}
    
    private void setVisibility(int id, int visibility) {
        final View v = mView.findViewById(id);
        if (v != null) {
            v.setVisibility(visibility);
        }
    }
    
    private void checkProxyValue(){
        boolean enable = false;
        final int selectedPosition = mProxySettingsSpinner.getSelectedItemPosition();
        if (selectedPosition == PROXY_STATIC) {
            String hostname = mHostname.getText().toString();
            String portStr = mPort.getText().toString();
            String exclusionlist = mExclusionlist.getText().toString();
            int result = 0;
            try {
                result = ProxySelector.validate(hostname, portStr, exclusionlist);
            } catch (NumberFormatException e) {
                result = R.string.proxy_error_invalid_port;
            }
            if (TextUtils.isEmpty(hostname)) {
                enable = false;
            } else {
                if (result == 0) {
                    enable = true;
                }else {
                    enable = false;
                }
            }
        } else if (selectedPosition == PROXY_PAC) {
            CharSequence uriSequence = mPac.getText();
            if (TextUtils.isEmpty(uriSequence)) {
                enable = false;
            } else {
                Uri uri = Uri.parse(uriSequence.toString());
                if (uri == null) {
                    enable = false;
                } else {
                    enable = true;
                }
            }
        }else {
            enable = true;
        }
        getButton(BUTTON_SUBMIT).setEnabled(enable);
    }
    
	@Override
	public void afterTextChanged(Editable s) {
        checkProxyValue();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// work done in afterTextChanged
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// work done in afterTextChanged
	}
    
        @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemSelected");
        showProxyFields();
        checkProxyValue();
        //enableSubmitIfAppropriate();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }

}
