#!/bin/bash
#
# Description   : Android Build Script.
# Authors       : www.9tripod.com
# Version       : 1.0
# Notes         : None
#

#
# JAVA PATH
#
export PATH=/usr/lib/jvm/java-1.7.0-openjdk-amd64/bin:$PATH

#
# Some Directories
#
BS_DIR_TOP=$(cd `dirname $0` ; pwd)
BS_DIR_TOOLS=${BS_DIR_TOP}/tools
BS_DIR_RELEASE=${BS_DIR_TOP}/out/release
BS_DIR_UBOOT=${BS_DIR_TOP}/u-boot
BS_DIR_KERNEL=${BS_DIR_TOP}/kernel
BS_DIR_TARGET=${BS_DIR_TOP}/out/target/product/rk3288

#
# Target Config
#
BS_CONFIG_BOOTLOADER_UBOOT=x3288_defconfig
BS_CONFIG_KERNEL=x3288_defconfig
#BS_CONFIG_KERNEL_DTB=x3288_cv4.1_bv4.img
BS_CONFIG_KERNEL_DTB=x3288_cv5_bv4.img
BS_CONFIT_FILESYSTEM=rk3288-userdebug
BS_CONFIG_FILESYSTEM=PRODUCT-rk3288-userdebug

setup_environment()
{
	LANG=C
	cd ${BS_DIR_TOP};

	PATH=${BS_DIR_TOP}/out/host/linux-x86/bin:$PATH;

	mkdir -p ${BS_DIR_RELEASE} || return 1
	[ -f "$BS_DIR_RELEASE/upgrade_tool" ] || { cp -v ${BS_DIR_TOP}/RKTools/linux/Linux_Upgrade_Tool_v1.2/upgrade_tool ${BS_DIR_RELEASE};}
	[ -f "$BS_DIR_RELEASE/config.ini" ] || { cp -v ${BS_DIR_TOP}/RKTools/linux/Linux_Upgrade_Tool_v1.2/config.ini ${BS_DIR_RELEASE};}
}

build_bootloader_uboot()
{
	# Compiler uboot
	cd ${BS_DIR_UBOOT} || return 1
	make distclean || return 1
	make ${BS_CONFIG_BOOTLOADER_UBOOT} || return 1
	make -j${threads} || return 1

	# Copy bootloader to release directory
	cp -av ${BS_DIR_UBOOT}/RK3288UbootLoader_V2.30.10.bin ${BS_DIR_RELEASE}/RK3288UbootLoader.bin || return 1

	echo "^_^ uboot path: ${BS_DIR_RELEASE}/uboot.bin"
	return 0
}

build_kernel()
{
	#export PATH=${BS_DIR_UBOOT}/tools:$PATH 
	# Compiler kernel
	cd ${BS_DIR_KERNEL} || return 1
	make ${BS_CONFIG_KERNEL} || return 1
	make ${BS_CONFIG_KERNEL_DTB} -j${threads} || return 1

	# Copy kernel.img & resource.img to release directory
	cp -v ${BS_DIR_KERNEL}/kernel.img ${BS_DIR_RELEASE}
	cp -v ${BS_DIR_KERNEL}/resource.img ${BS_DIR_RELEASE}

	return 0
}

build_system()
{
	cd ${BS_DIR_TOP} || return 1
	source build/envsetup.sh || return 1
	#make installclean
	make -j${threads} ${BS_CONFIG_FILESYSTEM} || return 1
	#lunch ${BS_CONFIT_FILESYSTEM} || return 1
	#make -j${threads} || return 1

	# create boot.img
	echo -n "create boot.img without kernel... "
	#cp -a ${BS_DIR_TARGET}/boot.img ${BS_DIR_RELEASE}/
	[ -d ${BS_DIR_TARGET}/root ] && \
	mkbootfs ${BS_DIR_TARGET}/root | minigzip > ${BS_DIR_TARGET}/ramdisk.img && \
	truncate -s "%4" ${BS_DIR_TARGET}/ramdisk.img && \
	rkst/mkkrnlimg ${BS_DIR_TARGET}/ramdisk.img ${BS_DIR_RELEASE}/boot.img >/dev/null
	echo "done."

	# create recovery.img
	echo -n "create recovery.img with kernel and with out resource... "
	[ -d ${BS_DIR_TARGET}/recovery/root ] && \
	mkbootfs ${BS_DIR_TARGET}/recovery/root | minigzip > ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	truncate -s "%4" ${BS_DIR_TARGET}/ramdisk-recovery.img && \
	mkbootimg --kernel ${BS_DIR_TARGET}/kernel --ramdisk ${BS_DIR_TARGET}/ramdisk-recovery.img --output ${BS_DIR_TARGET}/recovery.img && \
	cp -a ${BS_DIR_TARGET}/recovery.img ${BS_DIR_RELEASE}/
	echo "done."

	cp -av ${BS_DIR_TOP}/rkst/Image/misc.img ${BS_DIR_RELEASE} || return 1;
	cp -av ${BS_DIR_TOP}/rkst/Image/pcba_small_misc.img ${BS_DIR_RELEASE} || return 1;
	cp -av ${BS_DIR_TOP}/rkst/Image/pcba_whole_misc.img ${BS_DIR_RELEASE} || return 1;

	# create system.img
	if [ -d ${BS_DIR_TARGET}/system ]
	then
		echo -n "create system.img... "
		system_size=`ls -l ${BS_DIR_TARGET}/system.img | awk '{print $5;}'`
		[ $system_size -gt "0" ] || { echo "Please make first!!!" && exit 1; }
		MAKE_EXT4FS_ARGS=" -L system -S ${BS_DIR_TARGET}/root/file_contexts -a system ${BS_DIR_RELEASE}/system.img ${BS_DIR_TARGET}/system"
		ok=0
		while [ "$ok" = "0" ]; do
			make_ext4fs -l $system_size $MAKE_EXT4FS_ARGS >/dev/null 2>&1 &&
			tune2fs -c -1 -i 0 ${BS_DIR_RELEASE}/system.img >/dev/null 2>&1 &&
			ok=1 || system_size=$(($system_size + 5242880))
		done
		e2fsck -fyD ${BS_DIR_RELEASE}/system.img >/dev/null 2>&1 || true
		echo "done."
	fi

	return 0
}

build_update()
{
	#chmod a+r -R ${BS_DIR_RELEASE}/
	cd ${BS_DIR_RELEASE} || return 1

# generate update-android.img
	echo "create update-android.img..."
	# Copy package-file and parameter.txt
	cp -av ${BS_DIR_TOOLS}/parameter.txt ${BS_DIR_RELEASE} || return 1;
	cp -av ${BS_DIR_TOOLS}/package-file ${BS_DIR_RELEASE} || return 1;
	# Firmware pack
	${BS_DIR_TOP}/RKTools/linux/Linux_Pack_Firmware/rockdev/afptool -pack ${BS_DIR_RELEASE}/ ${BS_DIR_RELEASE}/temp.img || return 1;
	# Generating image
	${BS_DIR_TOP}/RKTools/linux/Linux_Pack_Firmware/rockdev/rkImageMaker -RK32 ${BS_DIR_RELEASE}/RK3288UbootLoader.bin ${BS_DIR_RELEASE}/temp.img ${BS_DIR_RELEASE}/update-android.img -os_type:androidos || return 1;
	rm -fr ${BS_DIR_RELEASE}/temp.img || return 1;
	echo "update-android.img is generated now!"

	return 0
}

threads=$(grep processor /proc/cpuinfo | awk '{field=$NF};END{print field+1}')
uboot=no
kernel=no
system=no
update=no

if [ -z $1 ]; then
	uboot=yes
	kernel=yes
	system=yes
	update=yes
fi

while [ "$1" ]; do
    case "$1" in
	-j=*)
		x=$1
		threads=${x#-j=}
		;;
	-u|--uboot)
		uboot=yes
	    ;;
	-k|--kernel)
	    	kernel=yes
	    ;;
	-s|--system)
		system=yes
	    ;;
	-U|--update)
		update=yes
	    ;;
	-cv=*)
		x=$1
		BOARD=${x#-cv=}
		echo "BOARD is ${BOARD}"
		if [ ${BOARD} = "5" ];then
			echo "change BS_CONFIG_KERNEL_DTB to x3288_cv5_bv4.img"
			BS_CONFIG_KERNEL_DTB=x3288_cv5_bv4.img
		fi
		if [ ${BOARD} = "4.1" ];then
			echo "change BS_CONFIG_KERNEL_DTB to x3288_cv4.1_bv4.img"
			BS_CONFIG_KERNEL_DTB=x3288_cv4.1_bv4.img
		fi
		;;
	-a|--all)
		uboot=yes
		kernel=yes
		system=yes
		update=yes
	    ;;
	-h|--help)
	    cat >&2 <<EOF
Usage: build.sh [OPTION]
Build script for compile the source of telechips project.

  -j=n                 using n threads when building source project (example: -j=16)
  -u, --uboot          build bootloader uboot from source
  -k, --kernel         build kernel from source
  -s, --system         build android file system from source
  -U, --update         build update file
  -cv                  select CoreBoard Version(example: cv=4.1, cv=5)
  -a, --all            build all, include anything
  -h, --help           display this help and exit
EOF
	    exit 0
	    ;;
	*)
	    echo "build.sh: Unrecognised option $1" >&2
	    exit 1
	    ;;
    esac
    shift
done

setup_environment || exit 1

if [ "${uboot}" = yes ]; then
	build_bootloader_uboot || exit 1
fi

if [ "${kernel}" = yes ]; then
	build_kernel || exit 1
fi

if [ "${system}" = yes ]; then
	build_system || exit 1
fi

if [ "${update}" = yes ]; then
	build_update || exit 1
fi

exit 0
