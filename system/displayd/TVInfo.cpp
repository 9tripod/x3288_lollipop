#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <fcntl.h>

#define LOG_TAG "RKTVINFO"
#include <cutils/log.h>

#include "TVInfo.h"

struct fb_chroma {
        __u32 redx;     /* in fraction of 1024 */
        __u32 greenx;
        __u32 bluex;
        __u32 whitex;
        __u32 redy;
        __u32 greeny;
        __u32 bluey;
        __u32 whitey;
};

struct fb_monspecs {
        struct fb_chroma chroma;
        struct fb_videomode *modedb;    /* mode database */
        __u8  manufacturer[4];          /* Manufacturer */
        __u8  monitor[14];              /* Monitor String */
        __u8  serial_no[14];            /* Serial Number */
        __u8  ascii[14];                /* ? */
        __u32 modedb_len;               /* mode database length */
        __u32 model;                    /* Monitor Model */
        __u32 serial;                   /* Serial Number - Integer */
        __u32 year;                     /* Year manufactured */
        __u32 week;                     /* Week Manufactured */
        __u32 hfmin;                    /* hfreq lower limit (Hz) */
        __u32 hfmax;                    /* hfreq upper limit (Hz) */
        __u32 dclkmin;                  /* pixelclock lower limit (Hz) */
        __u32 dclkmax;                  /* pixelclock upper limit (Hz) */
        __u16 input;                    /* display type - see FB_DISP_* */
        __u16 dpms;                     /* DPMS support - see FB_DPMS_ */
        __u16 signal;                   /* Signal Type - see FB_SIGNAL_* */
        __u16 vfmin;                    /* vfreq lower limit (Hz) */
        __u16 vfmax;                    /* vfreq upper limit (Hz) */
        __u16 gamma;                    /* Gamma - in fractions of 100 */
        __u16 gtf       : 1;            /* supports GTF */
        __u16 misc;                     /* Misc flags - see FB_MISC_* */
        __u8  version;                  /* EDID version... */
        __u8  revision;                 /* ...and revision */
        __u8  max_x;                    /* Maximum horizontal size (cm) */
        __u8  max_y;                    /* Maximum vertical size (cm) */
};

enum hdmi_video_color_mode {
	HDMI_COLOR_AUTO	= 0,
	HDMI_COLOR_RGB_0_255,
	HDMI_COLOR_RGB_16_235,
	HDMI_COLOR_YCBCR444,
	HDMI_COLOR_YCBCR422,
	HDMI_COLOR_YCBCR420
};

enum hdmi_deep_color {
	HDMI_DEPP_COLOR_AUTO = 0,
	HDMI_DEEP_COLOR_Y444 = 0x1,
	HDMI_DEEP_COLOR_30BITS = 0x2,
	HDMI_DEEP_COLOR_36BITS = 0x4,
	HDMI_DEEP_COLOR_48BITS = 0x8,
};

static int IsHDMIConnected(void)
{
	FILE *fd = fopen("/sys/class/display/HDMI/connect", "r");
	char buf[12];

	if(fd) {
		memset(buf, 0, 12);
		fgets(buf, 12, fd);
		fclose(fd);
		return atoi(buf);
	} else {
		return 0;
	}
}

static char *itoa(int val, char *buf, unsigned radix)
{
	char   *p;             
	char   *firstdig;      
	char	temp;           
	unsigned   digval;

	p = buf;
	if(val <0) {
		*p++ = '-';
		val = (unsigned long)(-(long)val);
	}
	firstdig = p; 
	do {
	digval = (unsigned)(val % radix);
	val /= radix;

	if (digval > 9)
	    *p++ = (char)(digval - 10 + 'a'); 
	else
	    *p++ = (char)(digval + '0');      
	} while(val > 0);

	*p-- = '\0 ';
	do {
		temp = *p;
		*p = *firstdig;
		*firstdig = temp;
		--p;
		++firstdig;        
	} while(firstdig < p);
	return buf;
}

int PortingOutputIoctl(HMW_HDMIRK_Ioctl_E op, HMW_VOID* arg)
{
	int fd;
	struct fb_monspecs monspecs;
	HMW_TV_INFO_S *tvinfo = (HMW_TV_INFO_S*)arg;

	if (arg == NULL || !IsHDMIConnected() || op != HMW_HDMI_RK_GET_TV_INFO) {
		ALOGE("arg %p, HDMI connect %d, OP %d", arg, IsHDMIConnected(), op);
		return -1;
	}
	FILE *ffd = NULL;
	char buf[32];
	ffd = fopen("/sys/class/display/HDMI/debug", "r");
	if (!ffd) {
		ALOGE("no hdmi debug node");
		return -1;
	}
	memset(buf, 0, 32);
	fgets(buf, 32, ffd);
	fclose(ffd);
	ALOGD("buf %s", buf);
	if (memcmp(buf, "EDID status:Okay", 16)) {
		ALOGE("EDID read failed");
		return -1;
	}
	fd = open("/sys/class/display/HDMI/monspecs", O_RDONLY);
	if (fd < 0) {
		ALOGE("open monspec failed");
		return -1;
	}
	unsigned int length = lseek(fd, 0L, SEEK_END);
	lseek(fd, 0L, SEEK_SET);
	if (length < sizeof(struct fb_monspecs))
		return -1;
	int len = read(fd, &monspecs, sizeof(struct fb_monspecs));
	close(fd);
	if (len != sizeof(struct fb_monspecs)) {
		ALOGE("read size is not eqaul to fb_monspecs");
		return -1;
	}
	memcpy(tvinfo->manufName, monspecs.manufacturer, 4);
	tvinfo->manufModel = monspecs.model;
	tvinfo->manufYear = monspecs.year;
	ALOGD("x %d y %d\n", monspecs.max_x, monspecs.max_y);
	int size = sqrt(monspecs.max_x * monspecs.max_x + monspecs.max_y * monspecs.max_y)/2.54 + 0.5;
	ALOGD("size %d\n", size);
	itoa(size, (char *)tvinfo->displaySize, 10);
	return 0;
}

enum {
	HDMI_COLORIMETRY_EXTEND_XVYCC_601,
	HDMI_COLORIMETRY_EXTEND_XVYCC_709,
	HDMI_COLORIMETRY_EXTEND_SYCC_601,
	HDMI_COLORIMETRY_EXTEND_ADOBE_YCC601,
	HDMI_COLORIMETRY_EXTEND_ADOBE_RGB,
	HDMI_COLORIMETRY_EXTEND_BT_2020_YCC_C, /*constant luminance*/
	HDMI_COLORIMETRY_EXTEND_BT_2020_YCC,
	HDMI_COLORIMETRY_EXTEND_BT_2020_RGB,
};

enum hdmi_hdr_eotf {
	EOTF_TRADITIONAL_GMMA_SDR = 1,
	EOFT_TRADITIONAL_GMMA_HDR = 2,
	EOTF_ST_2084 = 4,
};

int HdmiSupportedDataSpace(void)
{
	FILE *ffd = NULL;
	char buf[64];
	int colorimetry, dataspace = 0;
	unsigned int eotf;

	if (!IsHDMIConnected())
		return 0;

	ffd = fopen("/sys/class/display/HDMI/color", "r");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return 0;
	}
	memset(buf, 0, 64);
	while(fgets(buf, 64, ffd) != NULL) {
		if (!memcmp(buf, "Supported Colorimetry", 21)) {
			sscanf(buf, "Supported Colorimetry: %d", &colorimetry);
		} else if (!memcmp(buf, "Supported EOTF", 14)) {
			sscanf(buf, "Supported EOTF: 0x%x", &eotf);
		}
		memset(buf, 0, 64);
	}
	fclose(ffd);
	ALOGD("colorimetry %d, eotf 0x%x\n", colorimetry, eotf);
	if (colorimetry & ((1 << HDMI_COLORIMETRY_EXTEND_BT_2020_YCC) | (1 << HDMI_COLORIMETRY_EXTEND_BT_2020_RGB)))
		dataspace |= HAL_DATASPACE_STANDARD_BT2020;
	if (colorimetry & (1 << HDMI_COLORIMETRY_EXTEND_BT_2020_YCC_C))
		dataspace |= HAL_DATASPACE_STANDARD_BT2020_CONSTANT_LUMINANCE;

	if (eotf & EOTF_ST_2084)
		dataspace |= HAL_DATASPACE_TRANSFER_ST2084;
	return dataspace;
}

int HdmiSetHDR(int enable)
{
	FILE *ffd = NULL;
	char buf[64];
	int eotf;

	ffd = fopen("/sys/class/display/HDMI/color", "w");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return -1;
	}
	if (enable)
		eotf = EOTF_ST_2084;
	else
		eotf = 0;
	memset(buf, 0, 64);
	sprintf(buf, "hdr=%d", eotf);
	ALOGD("%s", buf);
	fwrite(buf, 1, strlen(buf), ffd);
	fclose(ffd);
	return 0;
}

int HdmiSetColorimetry(android_dataspace_t Colorimetry)
{
	FILE *ffd = NULL;
	char buf[64];
	char colorimetry;

	ffd = fopen("/sys/class/display/HDMI/color", "w");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return -1;
	}
	if (Colorimetry == HAL_DATASPACE_STANDARD_BT2020)
		colorimetry = HDMI_COLORIMETRY_EXTEND_BT_2020_YCC + 3;
	else if (Colorimetry == HAL_DATASPACE_STANDARD_BT2020_CONSTANT_LUMINANCE)
		colorimetry = HDMI_COLORIMETRY_EXTEND_BT_2020_YCC_C + 3;
	else
		colorimetry = 0;
	memset(buf, 0, 64);
	sprintf(buf, "colorimetry=%d", colorimetry);
	fwrite(buf, 1, strlen(buf), ffd);
	fclose(ffd);
	return 0;
}

int HdmiSupportedColorMode(void)
{
	FILE *ffd = NULL;
	char buf[64];
	int colormode, colordepth;
	int capacity = RGB_F_8 | RGB_L_8;

	if (!IsHDMIConnected())
		return 0;

	ffd = fopen("/sys/class/display/HDMI/color", "r");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return 0;
	}
	memset(buf, 0, 64);
	while(fgets(buf, 64, ffd) != NULL) {
		if (!memcmp(buf, "Supported Color Mode", 20)) {
			sscanf(buf, "Supported Color Mode: %d", &colormode);
		} else if (!memcmp(buf, "Supported Color Depth", 21)) {
			sscanf(buf, "Supported Color Depth: %d", &colordepth);
		}
		memset(buf, 0, 64);
	}
	fclose(ffd);
	ALOGD("colormode %d, colordepth 0x%x\n", colormode, colordepth);
	if (colordepth & (1 << HDMI_DEEP_COLOR_30BITS))
		capacity |= RGB_F_10 | RGB_L_10;
#ifdef SUPPORT_36BIT
	if (colordepth & (1 << HDMI_DEEP_COLOR_36BITS))
                capacity |= RGB_F_12 | RGB_L_12;
#endif
	if (colormode & (1 << HDMI_COLOR_YCBCR444)) {
		capacity |= Y444_8;
		if (colordepth & (1 << HDMI_DEEP_COLOR_30BITS))
			capacity |= Y444_10;
#ifdef SUPPORT_36BIT
		if (colordepth & (1 << HDMI_DEEP_COLOR_36BITS))
			capacity |= Y444_12;
#endif
	}
	if (colormode & (1 << HDMI_COLOR_YCBCR422)) {
		capacity |= Y422_8 | Y422_10 | Y422_12;
/*
		if (colordepth & (1 << HDMI_DEEP_COLOR_30BITS))
			capacity |= Y422_10;
#ifdef SUPPORT_36BIT
		if (colordepth & (1 << HDMI_DEEP_COLOR_36BITS))
			capacity |= Y422_12;
#endif
*/
	}
	
	if (colormode & (1 << HDMI_COLOR_YCBCR420)) {
		capacity |= Y420_8;
		if (colordepth & (1 << HDMI_DEEP_COLOR_30BITS))
			capacity |= Y420_10;
#ifdef SUPPORT_36BIT
		if (colordepth & (1 << HDMI_DEEP_COLOR_36BITS))
			capacity |= Y420_12;
#endif
	}
	ALOGD("%s capacity %x", __func__, capacity);
	return capacity;
}

int HdmiGetColorMode(void)
{
	FILE *ffd = NULL;
	char buf[64];
	int colormode, colordepth;
	int mode = 0;

	if (!IsHDMIConnected())
		return 0;

	ffd = fopen("/sys/class/display/HDMI/color", "r");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return 0;
	}
	memset(buf, 0, 64);
	while(fgets(buf, 64, ffd) != NULL) {
		if (!memcmp(buf, "Current Color Mode", 18)) {
			sscanf(buf, "Current Color Mode: %d", &colormode);
		} else if (!memcmp(buf, "Current Color Depth", 19)) {
			sscanf(buf, "Current Color Depth: %d", &colordepth);
		}
		memset(buf, 0, 64);
	}
	fclose(ffd);
	ALOGD("current colormode %d, colordepth 0x%x\n", colormode, colordepth);
	switch(colormode) {
		case HDMI_COLOR_RGB_0_255:
			mode = RGB_F_8;
			break;
		case HDMI_COLOR_RGB_16_235:
			mode = RGB_L_8;
			break;
		case HDMI_COLOR_YCBCR444:
			mode = Y444_8;
			break;
		case HDMI_COLOR_YCBCR422:
			mode = Y422_8;
			break;
		case HDMI_COLOR_YCBCR420:
			mode = Y420_8;
			break;
		default:
			break;
	}
	if (colordepth == 10)
		mode += 1;
	else if (colordepth == 12)
		mode += 2;
	else if (colordepth == 16)
		mode += 3;
	ALOGD("%s mode %d\n", __func__, mode);
	return mode;
}

#define HDMI_RGBF_MASK	0xf
#define HDMI_RGBL_MASK	0xf0
#define HDMI_Y444_MASK	0xf00
#define HDMI_Y422_MASK	0xf000
#define HDMI_Y420_MASK	0xf0000

#define HDMI_8BIT_MASK	(1 | (1 << 4) | (1 << 8) | (1 << 12))
#define HDMI_10BIT_MASK	(2 | (2 << 4) | (2 << 8) | (2 << 12))
#define HDMI_12BIT_MASK	(4 | (4 << 4) | (4 << 8) | (4 << 12))
#define HDMI_16BIT_MASK	(8 | (8 << 4) | (8 << 8) | (8 << 12))

int HdmiSetColorMode(int Colormode)
{
	FILE *ffd = NULL;
	char buf[64];
	int mode = 0;

	ffd = fopen("/sys/class/display/HDMI/color", "w");
	if (!ffd) {
		ALOGE("no hdmi color node");
		return -1;
	}
	if (Colormode & HDMI_RGBF_MASK)
		mode = HDMI_COLOR_RGB_0_255;
	else if (Colormode & HDMI_RGBL_MASK)
		mode = HDMI_COLOR_RGB_16_235;
	else if (Colormode & HDMI_Y444_MASK)
		mode = HDMI_COLOR_YCBCR444;
	else if (Colormode & HDMI_Y422_MASK)
		mode = HDMI_COLOR_YCBCR422;
	else if (Colormode & HDMI_Y420_MASK)
		mode = HDMI_COLOR_YCBCR420;
	if (Colormode & HDMI_8BIT_MASK)
		mode |= 8 << 8;
	else if (Colormode & HDMI_10BIT_MASK)
		mode |= 10 << 8;
	else if (Colormode & HDMI_12BIT_MASK)
		mode |= 12 << 8;
	else if (Colormode & HDMI_16BIT_MASK)
		mode |= 16 << 8;
	memset(buf, 0, 64);
	sprintf(buf, "mode=%d", mode);
	fwrite(buf, 1, strlen(buf), ffd);
	fclose(ffd);
	return 0;
}

#if 0
int main(int argc, char **argv)
{
	HMW_TV_INFO_S TVInfo;

	int rc = PortingOutputIoctl(HMW_HDMI_RK_GET_TV_INFO, &TVInfo);
	if (!rc) {
	printf("manufName %s\n", TVInfo.manufName);
	printf("manufModel %d\n", TVInfo.manufModel);
	printf("manufYear %d\n", TVInfo.manufYear);
	printf("displaySize %s\n", TVInfo.displaySize);
	} else
		printf("failed\n");

	HdmiSupportedDataSpace();

	int colormode = HdmiSupportedColorMode();
	if (colormode & RGB_F_8)
		printf("RGB_F_8\n");
	if (colormode & RGB_F_10)
		printf("RGB_F_10\n");
	if (colormode & RGB_L_8)
		printf("RGB_L_8\n");
	if (colormode & RGB_L_10)
		printf("RGB_L_10\n");
	if (colormode & Y444_8)
		printf("Y444_8\n");
	if (colormode & Y444_10)
		printf("Y444_10\n");
	if (colormode & Y422_8)
		printf("Y422_8\n");
	if (colormode & Y422_10)
		printf("Y422_10\n");

	HdmiSetColorMode(Y422_10);
	return 0;
}
#endif
